# This dockerfile creates a container of a truly basic LHCbDIRAC client installation.
# Based on CC7
#
# The purpose of this container can be:
# 1. for running a client, as simple as that
# 2. for debugging (interacting with LHCb production or certification environment)
#     for this second case you can keep the DIRAC code on your local machine,
#     and use only the DIRAC externals and lcg-bundle that sit on the container
#


# CC7 as base
FROM cern/cc7-base
MAINTAINER Federico Stagni <federico.stagni@cern.ch>

# install packages
RUN yum -y install tar && \
    yum install -y iputils && \
    yum install -y which && \
    yum clean all


# Create DIRAC dirs
RUN mkdir -p /opt/dirac && \
    mkdir -p /opt/dirac/etc/grid-security


# Installing LHCbDIRAC in /opt/dirac
RUN \
    cd /opt/dirac && \
    curl -L -o dirac-install https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/Core/scripts/dirac-install.py && \
    chmod +x dirac-install && \
    ./dirac-install -r v9r1p16 -l LHCb -e LHCb -g v13r0 && \
    rm -rf /opt/dirac/.installCache && \
    rm dirac-install

# Get a host cert/key in (used for running dirac-configure)
# (it assumes they are found in the local directory)
COPY hostcert.pem /opt/dirac/etc/grid-security/hostcert.pem
COPY hostkey.pem /opt/dirac/etc/grid-security/hostkey.pem
RUN chmod 440 /opt/dirac/etc/grid-security/hostcert.pem
RUN chmod 400 /opt/dirac/etc/grid-security/hostkey.pem

# Configuring LHCbDIRAC
# (it assumes the LHCbDIRACConfig.cfg is found in the local directory)
COPY LHCbDIRACConfig.cfg /opt/dirac/LHCbDIRACConfig.cfg
RUN source /opt/dirac/bashrc && \
    dirac-configure --UseServerCertificate /opt/dirac/LHCbDIRACConfig.cfg

# Get the user certificate files in, for creating proxy
# (it assumes they are found in the local directory)
RUN mkdir -p /home/root/.globus
COPY usercert.pem /root/.globus/usercert.pem
COPY userkey.pem /root/.globus/userkey.pem
RUN chmod 640 /root/.globus/usercert.pem
RUN chmod 400 /root/.globus/userkey.pem

# Copying in ENTRYPOINT script (for running DIRAC scripts directly, e.g. dirac-service)
COPY dockerEntrypoint.sh /opt/dirac/dockerEntrypoint.sh
RUN chmod 755 /opt/dirac/dockerEntrypoint.sh
ENTRYPOINT [ "/opt/dirac/dockerEntrypoint.sh" ]

# Copy the script so that when logging interactively the environment is correct
RUN cp /opt/dirac/bashrc /root/.bashrc

# Just standard working dir
WORKDIR /opt/dirac/

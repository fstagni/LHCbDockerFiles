VERY FIRST VERSION OF DOCKERFILE FOR INTERACTING WITH PRODUCTION ENVIRONMENT OF LHCb
====================================================================================


Build this with::
   
   docker build --network host -t devclient .

Then run it with::

   docker run -h localhost --network host -it -v /home/fstagni/pyDevs/DIRAC:/opt/dirac/DIRAC -v /home/fstagni/pyDevs/LHCbDIRAC/LHCbDIRAC/:/opt/dirac/LHCbDIRAC -v /home/fstagni/pyDevs/aaaRumenta:/opt/dirac/tests devclient bash

This will install v9r1p16 (see inside Dockerfile) and connect to LHCb-Certification setup by default (see LHCbDIRACConfig.cfg).



Needs: host certificate, and user certificate, all sitting in local dir.





TODO::

- use arguments to build and start https://stackoverflow.com/questions/34254200/how-to-pass-arguments-to-a-dockerfile
- make a wrapper around build + run
- publish "to the people"
